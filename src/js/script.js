// var slider = new Slider('#ex2', {}); 



// ПЕРЕКЛЮЧЕНИЕ ПУНКТОВ МЕНЮ
let itemsActive = function () {
    const navChildList = document.getElementsByClassName("top-menu__items");
    console.log(navChildList);
    for (let i = 0; i < navChildList.length; i++) {
        navChildList[i].addEventListener("mouseover", function (e) {


            for (let i = 0; i < navChildList.length; i++) {
                navChildList[i].classList.remove('top-menu__items--active');
                let parentElements = document.getElementsByClassName("nav-item");
                for (let i = 0; i < parentElements.length; i++) {
                    parentElements[i].classList.remove('top-menu__items--border');

                }
            };
            this.classList.add('top-menu__items--active');
            this.parentNode.classList.add('top-menu__items--border');

        })
    }
}
itemsActive();
// не сделанное выпадающее меню
let dropDown = function(){
  const menuBtn = document.querySelectorAll('.top-menu__dropdown-button');
  console.log(menuBtn);
}
dropDown();

$(".js_fg_trigger").click(function() {
    const tabName = $(this).attr("data-tab");
    const tab = $(`#${tabName}`);
    $(".js_fg_trigger.active").removeClass("active");
    $(this).addClass("active");
    $(".js_fg_content.active").removeClass("active");
    tab.addClass("active");
  });

// настройки слик-слайдера
$("#js-fgs-main1").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: "#js-fgs-nav1"
  });
  $("#js-fgs-nav1").slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    asNavFor: "#js-fgs-main1",
    dots: false,
    centerMode: false,
    focusOnSelect: true
  });

  // 2
  $("#js-fgs-main2").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: "#js-fgs-nav2"
  });
  $("#js-fgs-nav2").slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    asNavFor: "#js-fgs-main2",
    dots: false,
    centerMode: false,
    focusOnSelect: true
  });

  // 3
  $("#js-fgs-main3").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: "#js-fgs-nav3"
  });
  $("#js-fgs-nav3").slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    asNavFor: "#js-fgs-main3",
    dots: false,
    centerMode: false,
    focusOnSelect: true
  });

  // 4
  $("#js-fgs-main4").slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: false,
    fade: true,
    asNavFor: "#js-fgs-nav4"
  });
  $("#js-fgs-nav4").slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    asNavFor: "#js-fgs-main4",
    dots: false,
    centerMode: false,
    focusOnSelect: true
  });
