var gulp = require('gulp');
    autoprefixer = require('gulp-autoprefixer'),
    clean = require('gulp-clean'),
    sass = require('gulp-sass'),
    cleanCss = require('gulp-clean-css'),
    concat = require('gulp-concat'),
    imagemin = require('gulp-imagemin'),
    jsMinify = require('gulp-js-minify'),
    rigger = require('gulp-rigger'),
    uglify = require('gulp-uglify'),
    brouserSync = require('browser-sync'),
    reload = brouserSync.reload;

//     gulp.task('myTask', function(){
// console.log("Тест пройдено!");
//     });
   // Копирование файлов HTML в папку dist
    gulp.task("html", function(){
        return gulp.src("src/*.html")
            .pipe(rigger())
            .pipe(gulp.dest("build"))
    });
    // Объединение, компиляция Sass в CSS, простановка венд. префиксов
        gulp.task("sass", function(){
            return gulp.src("src/scss/*.scss")
                .pipe(concat('styles.scss'))
                .pipe(sass())
                .pipe(autoprefixer({
                    browsers:['last 2 version'],
                    cascade:false
                }))
                .pipe(gulp.dest("build/css"))
        })
        gulp.task("sass:watch", function () {
            gulp.watch('./sass/**/*.scss', ['sass']);
          });
    // // Минификация CSS файлов
    // gulp.task("minCss", function(){
    //     return gulp.src("src/scss/*.scss")
    //     .pipe(cleanCss())
    //     .pipe(rename({ suffix: '.min' }))
    //     .pipe(gulp.dest("build/css"))
    // })
    //Объединение и сжатие файлов JS
    gulp.task("script", function(){
        return gulp.src("src/js/*.js") //дериктория из которой берем все исходные файлы js
            .pipe(jsMinify())
            .pipe(concat('scripts.js')) //один общий объединенный файл JS
            .pipe(uglify())
           // .pipe(rename({suffix:'.min'})) 
            .pipe(gulp.dest("build/js"))
    })
    // Сжатие картинок
    gulp.task("imgs", function(){
        return gulp.src("src/images/**/*.+(jpg|jpeg|png|gif)")
            .pipe(imagemin({
                progrecive:true,
                svgoPlugins:[{removeVievBow: false}],
                interlaced: true
            }))
            .pipe(gulp.dest("build/images"))
    })
     gulp.task("default", ["html","sass","script","imgs","sass:watch"]);
